/*
 * zzllrr Mather
 * zzllrr@gmail
 * Released under MIT License
 */
sciences={index:['Universe','Life','Environment','Society'],

    'Universe':[
        'Physics',
        'Chemistry',

    ],
    'Life':[
        'Biology',
        'Intelligence',
        'Health',
        'Medicine',
        'Psychology'
    ],

    'Environment':[
        'Climate',
        'Polution',
        'Waste',
    ],



    'Society':[
        'Politics',
        'Economy',
        'Business',
        'Humanism',
        {'Subculture':[
            'Science Fiction',
            'Potential Science',
            'Pseudoscience',
            'Religion',
            'Superstition',
            'Anti-science',
        ]},
    ]

};